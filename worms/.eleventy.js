module.exports = eleventyConfig => {
  eleventyConfig.addPassthroughCopy({ "_static": "." });
  eleventyConfig.setDataDeepMerge(true);
  eleventyConfig.addShortcode("year", () => `${new Date().getFullYear()}`);

  return {
    dir: {
      output: "../public/worms"
    },
    templateFormats: ["njk"],
    pathPrefix: "/mirror/worms/",
  };
};
