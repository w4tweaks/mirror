module.exports = eleventyConfig => {
  eleventyConfig.addPassthroughCopy({ "_static": "." });
  eleventyConfig.setDataDeepMerge(true);

  return {
    dir: {
      output: "../public/worms3d"
    },
    templateFormats: ["njk"],
    pathPrefix: "/mirror/worms3d/",
  };
};
